cmake_minimum_required(VERSION 3.15)
project(razgovor_naloga)

set(CMAKE_CXX_STANDARD 14)

add_executable(razgovor_naloga
    main.cpp
    lib.cpp)