//
// Created by Miha Rozina on 6. 03. 2020.
//

#define _USE_MATH_DEFINES
#include <cmath>
#include <stdexcept>
#include <string>
#include "lib.h"


Generator& Generator::getInstance()
{
    static Generator instance;
    return instance;
}

void Generator::changePeakVoltage(float value)    { m_peakVoltage = value; }
float Generator::peakVoltage() const              { return m_peakVoltage; }

Amplifier& Amplifier::getInstance()
{
    static Amplifier instance;
    return instance;
}

void Amplifier::changeGain(Gain gain)   { m_gain = gain; }
Gain Amplifier::gain() const            { return m_gain; }

int Amplifier::gainConstant() const
{
    switch (m_gain)
    {
        case Gain::GAIN_1:      return 1;
        case Gain::GAIN_10:     return 10;
        case Gain::GAIN_100:    return 100;
        case Gain::GAIN_1000:   return 1000;
        case Gain::GAIN_10000:  return 10000;
    }

    throw std::runtime_error("Unknown gain");
}

Sampler& Sampler::getInstance()
{
    static Sampler instance;
    return instance;
}

void Sampler::start(Callback callback)
{
    if (m_isRunning)
    {
        throw std::runtime_error("Sampler is already running");
    }

    m_onSampleCallback = std::move(callback);
    m_isRunning = true;
    m_thread = std::thread([this]() {
        threadFunction();
    });
}

void Sampler::stop()
{
    m_isRunning = false;
    if (std::this_thread::get_id() != m_thread.get_id())
    {
        // called from outside m_thread
        if (m_thread.joinable()) m_thread.join();
    }
}

void Sampler::threadFunction()
{
    auto& gen = Generator::getInstance();
    auto& amp = Amplifier::getInstance();

    auto i = 0;

    for (;;)
    {
        auto peak = gen.peakVoltage() * static_cast<float>(amp.gainConstant());
        auto sample = std::sin(2 * M_PI * (i % 50) / 50) * peak;
        if (sample < -127) sample = -127;
        if (sample > 127) sample = 127;

        if (m_onSampleCallback) m_onSampleCallback(sample);

        if (!m_isRunning) break;

        ++i;
    }
}
