#include <iostream>
#include <vector>
#include <numeric>
#include <cmath>
#include "lib.h"

// Napiši program, ki povzorči 100 vzorcev signala in izračuna RMS vrednost. (RootMeanSquare)
// V konzolo izpiši (uporabi printf ali std::cout) RMS vrednost pri naslednjih peak vrednostih generatorja:
// - 100 V
// - 9 V
// - 250 mV
//
// Na voljo imaš naslednje komponente (glej lib.h za natančno dokumentacijo):
// - generator (Generator), ki generira sinusni signal z nastavljeno peak vrednostjo in tako frekvenco, da bo vzorčevalnik potreboval 50 vzorcev za eno periodo
// - ojačevalnik (Amplifier), ki omogoča nastavljanje ojačanja s petimi stopnjami ojačanja (glej enum Gain)
// - vzorčevalnik (Sampler), ki omogoča start / stop sempliranja in ti preko callback-a omogoča, da shraniš vsak posamezen vzorec
//
// Osnovni algoritem:
// - nastavi amplitudo generiranega signala
// - štartaj vzorčneje Sampler::start()
// - preko callback-a shranjuj vzorce
// - poišči pravi Gain pri katerem vzroci signala ne bo zabiti (vzročevalnik vrača vzorce v razponu [-127, 127])
// - ko imaš dovolj vzorcev izračunaj RMS vrednost in izpiši rezultat
//
// Dodatna navodila:
// - kode v lib.h in lib.cpp ne smeš spreinjati

const int16_t bufferValueNumber = 100;
std::vector<int> saveValue(bufferValueNumber);
bool bufferFull = false;
uint8_t count = 0;

void readValCB(int val)
{
    if(count >= 100){ // is Buffer full
        bufferFull = true; // set buffer to full
    }
    else{
        // std::cout<< val << std::endl; // check
        saveValue[count] = val; // buffer is not full, save value
    }
    ++count;

}

float rmsValue(std::vector<int> vecData, int lengthArr)
{
    float powerSum = 0, rms = 0;

    for (int i = 0; i < lengthArr; ++i) {powerSum += pow(vecData[i],2);};
    rms = sqrt(powerSum/lengthArr);
    return rms;
}

int main()
{
    Generator& generator = Generator::getInstance();
    Amplifier& amplifier = Amplifier::getInstance();
    Sampler& sampler = Sampler::getInstance();


    int exampleIndex = 0;
    float PeakVoltageValue[3] = {100.0,9.0,0.250};
    Gain gainAC[3] = {Gain::GAIN_1,Gain::GAIN_10,Gain::GAIN_100};

    for (exampleIndex = 0; exampleIndex < 3; ++exampleIndex) {

        generator.changePeakVoltage(PeakVoltageValue[exampleIndex]);
        std::cout << "generatorPeak: " << generator.peakVoltage() << "V" << std::endl;;

        amplifier.changeGain(gainAC[exampleIndex]);
        std::cout << "Gain: " << amplifier.gainConstant() << std::endl;;


        sampler.start(readValCB);
        while (!bufferFull) {};
        sampler.stop();

        /*
        std::cout << "Sampled data" << std::endl;;
        for (int i = 0; i < bufferValueNumber - 1; ++i) {
            std::cout<< saveValue.at(i) << std::endl;
        }
    `   */

        std::cout << "RMS: " << rmsValue(saveValue, bufferValueNumber) / amplifier.gainConstant() << " V \n" << std::endl;

        //allow to overwrite buffer
        bufferFull = false;
        count = 0;

    }


}